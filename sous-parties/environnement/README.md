# ConfigMap et Secrets : Paramétrer son environnement

<p align="center"><kbd >
<img style="border: 1px solid black;background-color:white"  src="https://matthewpalmer.net/kubernetes-app-developer/articles/configmap-diagram.gif"/>
</kbd></p>


<div style="color: #155724;background-color: #B3E5FC;border-color: #c3e6cb;">

ℹ️ Pour cette partie nous allons utiliser l'application présentée au tp1. 
Elle est disponible ici : https://github.com/Ragatzino/Initiation-docker-kubernetes. 
<br/>
</div>


> Pour la suite nous utiliserons les output de cette formation que sont les images docker de l'appli 
- sur tomcat : [ragatzino/initiation-docker-kube-tomcat](https://hub.docker.com/repository/docker/ragatzino/initiation-docker-kube-tomcat)
- et la base de données avec script d'init : [ragatzino/initiation-docker-kube-postgres](https://hub.docker.com/repository/docker/ragatzino/initiation-docker-kube-postgres)




## ConfigMap

Un ConfigMap permet la configuration clé valeur des variables d'environnements disponibles a un pod ou a un deploiement.

Pour des fins pratiques, nous avons intégré [un peu de code](https://github.com/Ragatzino/Initiation-docker-kubernetes/blob/main/src/main/java/fr/insee/sndioformationdockerkube/web/UserController.java#L58) a l'application spring boot tomcat.

=> En définitive, cela ajoute un endpoint /hello et la possibilité d'injecter une nouvelle variable d'environnement **HELLO_MESSAGE**

Nous allons donc vouloir valoriser cette variable d'environnement, et c'est justement ce que permet un ConfigMap.


```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: tomcatapp-configmap
data:
  HELLO_MESSAGE: bonjour
```
> fichier config-map-tomcatapp.yml

Ajoutez cette ressource en appliquant le fichier: 
```
kubectl apply -f ./sous-parties/environnement/assets/config-map-tomcatapp.yml
```

## Configurer un déploiement pour prendre en compte un configmap
Pour qu'un deploiement prenne en compte le configmap une option existe et peut être ajoutée dans la balise containers : 
```yaml
  containers:
    - configMapRef:
        name: tomcatapp-configmap
```

> Ces références sont courantes dans Kubernetes, elle se font par les métadonnées des ressources kubernetes => Ici metadata.name.tomcatapp-configmap

Observons ensemble l'application du fichier deployment-tomcatapp.yml

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: tomcat-app
spec:
  replicas: 1
  selector:
    matchLabels:
      app: tomcat-app
  template:
    metadata:
      labels:
        app: tomcat-app
    spec:
      containers:
        - name: tomcat-app
          image: ragatzino/initiation-docker-kube-tomcat:latest
          ports:
            - containerPort: 8080
          envFrom:
          - configMapRef:
              name: tomcatapp-configmap
``` 

> Verifications : 
Mettez le port du pod du deployment sur votre port 8080 : 

```
kubectl port-forward pods/tomcat-app<HASH> 8080:8080 
```

Puis accédez a http://localhost:8080/hello.


## Secrets

Les secrets permettent également une configuration, mais cette fois ci de manière chiffrée.


## Application pour la base de données Postgresql

Nous allons utiliser une BDD postgres, et l'on va configurer les mêmes variables d'environnement que dans le TP1.

RAPPEL:
```
docker run -d \
  --name=postgres-app \
  -e POSTGRES_USER=postgres \
  -e POSTGRES_DB=postgres \
  -e POSTGRES_PASSWORD=mdp \
  -p 5432:5432 \
  postgres:11
```

Ici on voudra donc cacher le mot de passe **POSTGRES_PASSWORD** et créer un configmap pour les variables POSTGRES_USER et POSTGRES_DB.

Cela donne : 
```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: database-configmap
data:
  POSTGRES_USER: postgres
  POSTGRES_DB: postgres
```
> database-configmap.yml

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: database-secret
type: Opaque
data:
  POSTGRES_PASSWORD: mdp
```
> database-secret.yml

- Appliquez ces fichiers 

```
kubectl apply -f ./sous-parties/environnement/assets/database-configmap.yml
```

```
kubectl apply -f ./sous-parties/environnement/assets/database-secret.yml
```

De la même manière que pour les configmap, on peut injecter les valeurs des secrets avec l'ajout sous la balise containers : 

```yaml
envFrom:
    - secretref:
        name: database-secret
```

## Valorisation des variables d'environnement partagées

Dans le cas de variables partagées, on peut affecter les valeurs a d'autres clés, par exemple si l'on veut que les variables affectées côté base de données soient récupérées pour valoriser le tomcat. 

Exemple : 

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: secret-env-pod
spec:
  containers:
  - name: app
    image: ragatzino/initiation-docker-kube-tomcat:latest
    env:
      - name: SPRING_DATASOURCE_USERNAME
        valueFrom:
          secretKeyRef:
            name: database-configmap
            key: POSTGRES_USER
      - name: SPRING_DATASOURCE_PASSWORD
        valueFrom:
          secretKeyRef:
            name: database-secret
            key: POSTGRES_PASSWORD
```

> Nous lancerons le postgres dans une prochaine partie, il s'agit d'une application a état, et Kubernetes propose une ressource de type StatefulSet pour cela.

## [Retour a la page principale ↩️ ](../../README.md)
