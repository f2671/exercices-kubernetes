# Fil rouge

Ici nous allons finaliser l'installation d'un tomcat et d'un postgres configurés dans Kubernetes en reprenant tous les concepts précédents.

Si l'on se rappelle, il y a donc :

Pour le Tomcat : 
- Un **Deployment** pour déployer des tomcats
- Un **ConfigMap** pour y stocker nos properties propre au tomcat ou équivalent
- Un **Service** pour exposer le tomcat a l'intérieur du cluster
- Un **Ingress** pour faire le pont et donner une URL propre

Pour la BDD : 
- Un **StatefulSet** pour déployer un postgres, demander un espace persistant et s'y brancher
- Un **ConfigMap** & un **Secret** pour y stocker nos variables d'environnement sensibles ou non
- Un **Service** pour exposer la bdd au tomcat

> Si vous voulez nettoyer votre **namespace** courant
```
kubectl delete --all all
```


Vous pouvez lancer qui se connecte a la base de données en 2 commandes :

Base de données : 
```
kubectl apply -f ./sous-parties/fil-rouge/postgres/
```
Tomcat : 
```
kubectl apply -f ./sous-parties/fil-rouge/tomcat/
```

Vous pouvez aussi configurer un ingress pour le déployer sur l'url de votre choix
## Application

On va d'abord installer la base de données, le tomcat en dépendant.

### Base de données

On commence par les variables d'environnement : 
```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: postgresapp-configmap
data:
  POSTGRES_USER: postgres
  POSTGRES_DB: postgres
```
> postgres-app-configmap.yaml

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: postgresapp-secret
type: Opaque
stringData:
  POSTGRES_PASSWORD: mdp
```
> postgres-app-secret.yaml

Puis on voudrait configurer la base de données branchée sur son disque et avec ses variables d'environnement.

```yaml
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: postgres-app
spec:
  selector:
    matchLabels:
      app: postgres-app
  serviceName: postgres-app-service
  replicas: 2
  template:
    metadata:
      labels:
        app: postgres-app
    spec:
      containers:
      - name: postgres-app
        image: ragatzino/initiation-docker-kube-postgres:latest
        ports:
        - containerPort: 5432
        volumeMounts:
        - name: disque-postgres
          mountPath: /data
        env:
          # rapport au montage du disque sur /data
          - name: PGDATA
            value: /data/pgdata
        envFrom:
          - configMapRef:
              name: postgresapp-configmap
          - secretRef:
              name: postgresapp-secret

  volumeClaimTemplates:
  - metadata:
      name: disque-postgres
    spec:
      accessModes: [ "ReadWriteOnce" ]
      resources:
        requests:
          storage: 1Gi
```
> postgres-app-statefulset.yaml

Il faut également un service pour rendre accessible la bdd : 

```yaml
apiVersion: v1
kind: Service
metadata:
  name: postgres-app-service
spec:
  selector:
    app: postgres-app
  ports:
  - port: 5432
    targetPort: 5432
```
> postgres-app-service.yaml

On peut maintenant y accéder par postgres-app-service:5432


**LANCEMENT DU POSTGRES**

On peut appliquer plusieurs fichiers en même temps en mettant un dossier au lieu d'un fichier.

```
kubectl apply -f ./sous-parties/fil-rouge/assets/postgres/
```
### Tomcat

On veut d'abord ajouter les variables d'environnement propres au tomcat dans un configmap.

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: tomcatapp-configmap
data:
  HELLO_MESSAGE: bonjour
  SPRING_H2_CONSOLE_ENABLED: "false"
  SPRING_DATASOURCE_URL: "jdbc:postgresql://postgres-app-service:5432/postgres"
  SPRING_DATASOURCE_DRIVERCLASSNAME: "org.postgresql.Driver"
  SPRING_JPA_DATABASE-PLATFORM: "org.hibernate.dialect.PostgreSQLDialect"
```
> tomcatapp-configmap.yaml

Le **Service** aui permet d'exposer le tomcat

```yaml
apiVersion: v1
kind: Service
metadata:
  name: tomcatapp-service
spec:
  type: LoadBalancer
  selector:
# metadata labels du deployment par ex
    app: tomcatapp-deployment
  ports:
    - name: http
      protocol: TCP
      port: 80
      targetPort: 8080
```
> tomcatapp-service.yaml

Un **Ingress** pour faire le pont vers l'exterieur et donner une URL propre

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: tomcatapp-ingress
  labels:
    name: tomcatapp-ingress
spec:
  rules:
  - host: tomcatapp-demo.dev.insee.io
    http:
      paths:
      - pathType: Prefix
        path: "/"
        backend:
          service:
          ## metadata du service
            name: tomcatapp-service
            port: 
              number: 8080
```

> tomcatapp-ingress.yaml

Et un déploiement qui permet de gérer la vie de 3 tomcats configurés depuis l'environnement

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: tomcatapp-deployment
spec:
  replicas: 3
  selector:
    matchLabels:
      app: tomcatapp-deployment
  template:
    metadata:
      labels:
        app: tomcatapp-deployment
    spec:
      containers:
        - name: tomcatapp-container
          image: ragatzino/initiation-docker-kube-tomcat:latest
          ports:
            - containerPort: 8080
          envFrom:
          - configMapRef:
              name: tomcatapp-configmap
          env:
          - name: SPRING_DATASOURCE_USERNAME
            valueFrom:
              configMapKeyRef:
                name: postgresapp-configmap
                key: POSTGRES_USER
          - name: SPRING_DATASOURCE_PASSWORD
            valueFrom:
              secretKeyRef:
                name: postgresapp-secret
                key: POSTGRES_PASSWORD
```

> tomcatapp-deployment.yaml

**LANCEMENT DU TOMCAT**


```
kubectl apply -f ./sous-parties/fil-rouge/assets/tomcat/
```

Vous pouvez appliquer l'ingress de votre choix



## Mais ça fait beaucoup de duplication tout ça

On a du se rappeler que certains selecteurs correspondaient au label etc.. Une solution "confort" serait de variabiliser tout ça pour éviter d'avoir a se rappeler "le service fait référence au déploiement par son label" . C'est en ce sens que l'on travaillera la semaine prochaine, avec HELM.

## [Retour a la page principale ↩️ ](../../README.md)
