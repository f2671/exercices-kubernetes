# Application à état : Stateful Set

<p align="center"><kbd >
<img src="https://s7280.pcdn.co/wp-content/uploads/2019/03/Kubernets-StatefulSets-Usecases.png" />
</kbd></p>


Nous allons ici nous intéresser a une partie analogue a la partie sur les volumes du TP précédent.

Quel est notre besoin ici ?

- Déployer une bdd PostgreSQL 11 initialisée avec des données et si elle meurt, la relancer en la branchant sur les données précédentes.

Il faut donc demander a kubernetes de mettre en place un volume qui permettra a postgresql d'écrire a un endroit persisté.

> PostgreSQL fonctionne avec le repertoire /data/ dans lequel il stocke vos données.

```yaml
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: postgres-app
spec:
  selector:
    matchLabels:
      app: postgres-app
  serviceName: postgres-app-service
  replicas: 1
  template:
    metadata:
      labels:
        app: postgres-app
    spec:
      containers:
      - name: postgres-app
        image: ragatzino/initiation-docker-kube-postgres
        ports:
        - containerPort: 5432
        volumeMounts:
        - name: disque-postgres
          mountPath: /data
  volumeClaimTemplates:
  - metadata:
      name: disque-postgres
    spec:
      accessModes: [ "ReadWriteOnce" ]
      resources:
        requests:
          storage: 1Gi
```
> postgresql-stateful-set.yaml

## Detail 

```yaml
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: postgres-app
spec:
  selector:
    matchLabels:
      app: postgres-app
  serviceName: postgres-app-service
  replicas: 1
  template:
    metadata:
      labels:
        app: postgres-app
    spec:
      containers:
      - name: postgres-app
        image: ragatzino/initiation-docker-kube-tomcat
        ports:
        - containerPort: 5432
```

Ici l'on serait presque dans un cas classique de **Deployment**. La partie nouvelle et interessante se trouve en bas.

```yaml
        volumeMounts:
        - name: disque-postgres
          mountPath: /data
  volumeClaimTemplates:
  - metadata:
      name: disque-postgres
    spec:
      accessModes: [ "ReadWriteOnce" ]
      resources:
        requests:
          storage: 1Gi
```
Lorsque vous désirez travailler avec des disques, il faut faire des **Persistant Volume Claim** ou **PVC**. En gros demander a Kubernetes de nous préter gentillement de la ressource, cette demande peut être refusée s'il n'a plus rien en stock.

On va utiliser ce volume avec l'option volumeMounts sur les pods PostgreSQL.

## Appliquer la ressource

```
kubectl apply -f ./sous-parties/application-a-etat/assets/postgresql-stateful-set.yaml
```

## [Retour a la page principale ↩️ ](../../README.md)