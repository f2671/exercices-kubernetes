# Accès aux pods depuis l'extérieur

Les pods sont à l'intérieur du réseau interne de Kubernetes. Par défaut ils n'interagissent pas sur l'hôte sur lequel ils ont été hébergé.

<p align="center"><kbd>
<img style="border: 1px solid black" src="https://banzaicloud.com/blog/k8s-ingress/ingress-host-based.png" width="600" />
</kbd>
</p>

## Service 

Kubernetes propose des ressources permettant de relier au niveau réseau des pods entre eux, cela peut permettre la communication entre différents pods.


> Cela permet de créer des "Network" de manière analogue a ce que l'on a pu créer côté Docker.

```yaml
apiVersion: v1
kind: Service
metadata:
  name: tomcat-example
spec:
  type: LoadBalancer
  selector:
# metadata labels du deployment par ex
    app: tomcat-example
  ports:
    - name: http
      protocol: TCP
      port: 80
      targetPort: 8080
```

Nous avons configuré un fichier qui lance un deployment d'un tomcat simple et permet d'accéder via une ressource.

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: tomcat-example-deployment
  labels:
    app: tomcat-example
spec:
  replicas: 3
  selector:
    matchLabels:
      app: tomcat-example
  template:
    metadata:
      labels:
        app: tomcat-example
    spec:
      containers:
      - name: tomcat-container
        image: tomcat:9-jre11
        ports:
        - containerPort: 8080
---

apiVersion: v1
kind: Service
metadata:
  name: tomcat-example-service
spec:
  type: LoadBalancer
  selector:
# metadata labels du deployment par ex
    app: tomcat-example
  ports:
    - name: http
      protocol: TCP
      port: 80
      targetPort: 8080
```
> tomcat-example-with-service.yaml

Pour le lancer : 
```
kubectl apply -f tomcat-example-with-service.yaml
```

> Bonjour Kubernetes, je voudrais créer une ressource accessible sur le cluster qui repartit la charge sur 3 tomcat.

Vous pouvez verifier qu'il est bien apparu 3 pod, un deployment et un service : 
```
kubectl get pods
```

```
kubectl get service
```

> Les ressources de type service ont un CLUSTER-IP et un PORT pour l'accès interne depuis le cluster.

## Ingress

Pour l'accès, nous allons opter pour une exposition de IP:port interne vers l'exterieur pour pouvoir y accéder directement depuis notre poste.

Kubernetes propose pour cela un concept: l'Ingress.

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: tomcat-example-ingress
  labels:
    name: tomcat-example-ingress
spec:
  rules:
   ## Hote ex : "toto.kube.developpement.insee.fr" en interne ou "toto.dev.insee.io" sur le cluster insee io
  - host: <Host>
    http:
      paths:
      - pathType: Prefix
        path: "/"
        backend:
          service:
          ## metadata du service
            name: tomcat-example-service
            port: 
              number: 8080
```
> ingress-tomcat-example-service.yaml

Vous pouvez modifier le fichier pour y mettre le nom d'url que vous souhaitez pour votre ingress.

> En traduction : Kubernetes, tu peux exposer le service tomcat-example-service 8080 sur l'URL toto.kube.developpement.insee.fr

Paramétrez le fichier pour une URL de votre choix, et utilisez la configuration du fichier : 

```
kubectl apply -f ingress-tomcat-example-service.yaml
```

Vous pouvez maintenant verifier que le fichier est bien appliqué : 

```
kubectl get ingress
```

Et accéder a votre ressource depuis votre navigateur ex : https://toto.kube.developpement.insee.fr/ (interne)

## Objectif pour notre application

- Nous voulons que le tomcat se connecte a une base de données et donc nous allons créer un service dédié a cette base de données.

Cela donne : 
```yaml
apiVersion: v1
kind: Service
metadata:
  name: postgres-app-service
spec:
  selector:
    app: postgres
  ports:
  - port: 5432
    targetPort: 5432
```

> postgres-service-cible.yaml
- Nous voulons que le tomcat soit accessible depuis l'exterieur du cluster, nous allons donc créer un service pour le rendre accessible puis un ingress pour le rendre disponible depuis l'exterieur.

Cela donne :
```yaml
apiVersion: v1
kind: Service
metadata:
  name: app-service
spec:
  type: LoadBalancer
  selector:
# metadata labels du deployment par ex
    app: app
  ports:
    - name: http
      protocol: TCP
      port: 80
      targetPort: 8080

---

apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: app-ingress
  labels:
    name: app-ingress
spec:
  rules:
  - host: app.kube.developpement.insee.fr
    http:
      paths:
      - pathType: Prefix
        path: "/"
        backend:
          service:
          ## metadata du service
            name: app-service
            port: 
              number: 8080
```
> tomcat-service-and-ingress-cible.yaml

Nous allons maintenant nous interesser au cas particulier des systèmes persistants : 
## [Retour a la page principale ↩️ ](../../README.md)