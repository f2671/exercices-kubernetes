# Sous parties

Le support a été séparé en sous parties pour plusieurs raisons : 

- Permettre de clarifier et de ne pas surcharger un fichier README
- Favoriser une navigation 
- Permettre de donner les fichiers de configuration pour la sous partie, ce qui permet donc d'executer de son côté différentes commandes.

> Remarque : Les fichier seront stockés dans /sous-partie/$NOM_SOUS_PARTIE/assets/

## Sommaire

-  [authentification](/sous-parties/authentification/README.md)
-  [premier deployment](/sous-parties/premier-deployment/README.md)
-  [environnement](/sous-parties/environnement/README.md)
- [communication](exterieur/README.md)
-  [application a état](/sous-parties/application-a-etat/README.md)
- [bilan](fil-rouge/README.md)