# Déploiements et premières commandes sur le cluster

L'idée de cette partie est de travailler au deploiement de nos premiers pods sur le cluster kubernetes.

Un pod = un processus, une brique unitaire de tout ce qui peut se passer sur kubernetes.

## Pod : La brique unitaire du cluster

<p align="center"><kbd s>
<img style="border: 1px solid black;background-color:white" src="https://www.padok.fr/hubfs/Images/Blog/kubernetes-Pod.png" width="600" />
</kbd>
</p>

Pour cette partie, nous allons utiliser l'image gcr.io/google-samples/node-hello:1.0 comme application, il s'agit d'un serveur node JS qui expose hello world.

Pour lancer une image sans configuration particulière on peut utiliser kubectl run : 

```
kubectl run tomcat --image=tomcat:9-jre11
```

Dans les faits, vous serez en général amenés a appliquer des fichiers de configuration **YAML**

> Lancez la commande kubectl run : cela deploiera l'application

Vous pouvez observer les applications qui tournent dans votre espace de travail avec la commande : 

```
kubectl get pods
```

Utilisez là pour observer la liste de toutes les applications déployées sur votre espace de travail

> Cette commande très utile, s'étend pour toutes les ressources de kubernetes

## Exemple de fichier yaml

Vous trouverez dans sous-parties/premier-deployment/assets/pod-exemple.yaml un premier fichier yaml pour lancer un pod.

```
cat sous-parties/premier-deployment/assets/pod-exemple.yaml
```

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: tomcat-exemple
spec:
  containers:
    - name: tomcat-exemple
      image: tomcat:9-jre11
      ports:
        - containerPort: 8080
```

Essayons de traduire ce fichier : 
Bonjour, je suis un pod qui s'appelle **tomcat-exemple**, je suis constitué d'un conteneur docker, qui s'appelle **tomcat-exemple** et qui est un **tomcat:9-jre11** (image docker tomcat 9 avec jre 11). J'expose le port du conteneur 8080 a l'interieur du pod.


## Appliquer un fichier
La commande que vous utiliserez souvent pour executer des fichiers est : 

```
kubectl apply -f <chemin-du-fichier>
```

> Cela se traduit à : Cluster pourrais tu installer la ressource définie dans le fichier \<chemin-du-fichier> ?

Testez par exemple 

```
kubectl apply -f sous-parties/premier-deployment/assets/pod-exemple.yaml
```

Vous pouvez toujours vérifier par :

```
kubectl get pods
```

> Note : vous pouvez utiliser la commande kubectl get all pour afficher toutes les ressources d'un espace de travail courant.


## Deployment

Le deployment est un type de ressource de kubernetes, il permet de définir un environnement stable cible pour notre application.


<p align="center"><kbd>
<img style="border: 1px solid black;background-color:white" src="https://matthewpalmer.net/kubernetes-app-developer/articles/kubernetes-deployment-static.png" width="600" />
</kbd>
</p>

Observons par exemple le fichier deployment suivant

```
cat sous-parties/premier-deployment/assets/deployment-exemple.yaml
```

Appliquons le sur le cluster

```
kubectl apply -f sous-parties/premier-deployment/assets/deployment-exemple.yaml
```

```
kubectl get all
```

=> 1 deployment et 1 pod qui tourne






## [Retour a la page principale ↩️ ](../../README.md)