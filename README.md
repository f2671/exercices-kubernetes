# Exercices Kubernetes

Ces exercices sont liés au TP2 de la session d'ateliers sur Docker, Kubernetes et Helm.

L'enjeu est ici de pratiquer et de s'initier au travail sur cluster Kubernetes. Nous évoquerons certains concepts spécifiques et avons comme objet un fil rouge précisé lors de la première séance : Héberger une application - Tomcat / BDD Postgresql sur un cluster Kubernetes.

Lien vers les slides : https://f2671.gitlab.io/formation-docker-kube-support/formation-docker-kube-2022#/4

## Prérequis d'installation 

- Installation du client pour l'api kubernetes **kubectl** (windows) :
  - Télécharger la dernière version du client ici : https://dl.k8s.io/release/v1.23.0/bin/windows/amd64/kubectl.exe
  - Déplacer ce .exe téléchargé dans un endroit convenable (e.g. ~/programmes ou dans C:/INSEE par ex)
  - Ajouter à la variable d'environnement PATH une entrée qui pointe vers le répertoire qui contient le .exe (permet a windows de le retrouver)
- Création d'un compte SSPCLOUD : https://dev.insee.io/ ou https://datalab.sspcloud.fr/ (si ce n'est pas déjà fait et si vous souhaitez travailler sur un des 2 clusters de la DIIT)


### Validation  
Validez votre installation en tapant depuis une invite de commande après avoir rafraichî vos variables d'environnement : 
```
kubectl version
```


## 0 - Accéder à un cluster Kubernetes : Authentification

Cette partie précise un élément pourtant important avant de travailler sur un cluster kubernetes, l'authentification.

Elle propose des pistes pour l'authentification sur les clusters INSEE actuels.

- [Accéder à la sous partie ▶️](sous-parties/authentification/)


## Rappel d'architecture 

Côté utilisateur d'un cluster kubernetes, vous interagissez avec le Control plane, qui connait l'état des différentes briques applicatives (pods) du cluster et sait allouer les ressources sur les différents noeuds x **worker** du cluster.

Vos déploiements et conteneurs seront exécutés sur différents noeuds du cluster, vous pourrez les y retrouver.
<p align="center"><kbd>
<img src="https://platform9.com/wp-content/uploads/2019/05/kubernetes-constructs-concepts-architecture-610x476.jpg" width="600"/>
</kbc>
</p>

Vous interagirez tout au long de ce TP avec un cluster Kubernetes par le biais du client en ligne de commande pour api kubernetes **kubectl**.


En amont du travail sur les parties sur le cluster, nous vous proposons de cloner ce dépot git.

```
git clone https://gitlab.com/f2671/exercices-kubernetes.git 
```

Il comprend le support ainsi que des fichiers préconfigurés pour une application facilitée.

**Pour la suite du support, nous allons nous servir de kubectl et de commandes, une fiche commandes souvent utilisées ici : https://kubernetes.io/docs/reference/kubectl/cheatsheet/**

## 1 - Deployment et premières commandes sur le cluster

Une première approche consiste à vouloir reproduire ce que l'on a pu voir dans le TP précédent : faire tourner des images docker. 

Mais cette fois ci, sur le cluster et pour cela, un concept : le **Deployment**.

> Remarque : ce sont des pods mais l'on est pas trop loin. (cf : [lien](https://iximiuz.com/en/posts/containers-vs-pods/))


- [Accéder a la sous partie ▶️](sous-parties/premier-deployment/)

## 2 - Lister des variables d'environnement : ConfigMap et Secrets

Pour une application, en fonction de la cible, on est toujours amenés à configurer les environnements.

Il faut savoir que cela fait partie d'un très bon pattern de développement pour la portabilité applicative : https://12factor.net/fr/config

Pour cela, Kubernetes propose la configuration de variable d'environnement cachés ou non par les concepts de **ConfigMap** et **Secret**.

- [Accéder a la sous partie ▶️](sous-parties/environnement/)


## 3 - Exposer ses conteneurs à l'exterieur : Services - Ingress


Une fois que l'on a réussi à faire tourner des pods, on peut s'intéresser à leur accès : 

> Qu'est ce qui ferait que la fois d'après, il sera sur la même IP

Pour cela Kubernetes gère un réseau interne qu'il rend disponible au travers de la notion de Service au sein du cluster.

Si l'on souhaite accéder à sa ressource interne à l'extérieur du réseau interne, l'Ingress permet d'exposer des routes externes aux services interne. (c'est plus joli ✨)


- [Accéder à la sous partie ▶️](sous-parties/exterieur/)

## 4 - Application à état : StatefulSet

Pour le cas d'application a état, on est amené a travailler avec des Volumes comme côté Docker.
Il faut également que ses volumes soient persistés et bien branchés au redémarrage, un processus n'étant pas fait pour vivre éternellement.

Kubernetes propose un type de ressource pour ce cas d'usage : le **StatefulSet**

- [Accéder a la sous partie ▶️](sous-parties/application-a-etat/)


## 5 - Le ticket si@moi

L'objectif de cette partie est de gagner du temps et de demander à Kubernetes de mettre en place une plateforme de base, avec un tomcat 9 qui tourne avec un jdk 11.

Vous allez d'ailleurs demander une autre plateforme, une base de données Postgres 11.

> On est bien sûr sur un cas d'exemple, la configuration sera probablement plus touffue, de plus que vos applications auront besoin de réseau, mais l'idée est là.


- [Accéder à la sous partie ▶️](sous-parties/fil-rouge/)


